package com.mkyong.examples;
public class MessageBuilder {

    public static String getHelloWorld(){
        return "hello world";
    }

    public static int getNumber10(){
        return 10;
    }

    public static char getLastAlpha(){
        return 'z';
    }

    public static String getName(){
        return "shashwat";
    } 

    public static int getId(){
        return 1;
    }
}
