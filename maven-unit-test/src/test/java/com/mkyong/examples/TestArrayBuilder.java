package com.mkyong.examples;

import org.junit.jupiter.api.Test;

// import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TestArrayBuilder {

    @Test
    public void testHelloWorld() {
        int expected[] = {1,2,2,3,5,6};
        assertArrayEquals(expected, ArrayBuilder.getSortArray());
    }

}