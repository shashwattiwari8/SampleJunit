package com.mkyong.examples;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMessageBuilder {

    @Test
    public void testHelloWorld() {
        assertEquals("hello world", MessageBuilder.getHelloWorld());
    }

    @Test
    public void testNumber10() {
        assertEquals(10, MessageBuilder.getNumber10());
    }

    @Test
    public void testLastAlpha(){
        assertEquals('z', MessageBuilder.getLastAlpha());
    }

    @Test
    public void testName(){
        assertEquals("shashwat", MessageBuilder.getName());
    }

    @Test
    public void testId(){
        assertEquals(1,MessageBuilder.getId());
    }

}